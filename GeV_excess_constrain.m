function [ff,num_detectable_MSPs,ee,...
    log_ellip_dist,ellip_kde,ellip_pdf,...
    log_f_dist,f_kde,f_pdf,...
    fs_UL,ellip_UL,log_crust_dist,xmesh_u,density_u,L0,sigmaL,Nmsp,N_msps_int_over_f] =...
    GeV_excess_constrain(ellip_dist,search,GC_dist,prop_factor,Izz,plot_flag,sdlim_red_fact,lum_func)
%GeV_excess_constrain 
% 
% This function takes as input a type of ellipticity distribution, upper
% limits from a CW search, a distance, a proportionality factor between
% neutron star external and internal B fields, and a moment of inertia. 
% 
% The output of this code are constraints on the number of detectable MSPs,
% integrated over the ellipticity distribution, as a function of frequency,
% and the number of detectable pulsars, integrated over the ATNF frequency
% distribution of MSPs.
% 
%   -----------------------------INPUTS:-----------------------------------
%   ellip_dist:     name of ellipticity distribution to use (string)
%   search:         which search to load upper limits from (string)
%   GC_dist:        distance to compute ellipticity upper limits at (kpc)
%   prop_factor:    ratio of internal to external B field (number >=1)
%   Izz:            moment of inertia (kg *m^2)
%   plot_flag:      to make certain plots (all plots: 2; some: 1; none: 0)
% 


if ~exist('ellip_dist','var')
    ellip_dist='atnf';
end

if ~exist('search','var')
    search='gcL';
end

if ~exist('GC_dist','var')
    GC_dist=8;%kpc
end

if ~exist('prop_factor','var')                                              % proportionality factor b/t internal B and surface B
    prop_factor=1;
end

if ~exist('Izz','var')
    Izz=10^38;                                                              % fiducial value, 1.4Msun, 10 km rad star for I=2/5*MR^2
end

if ~exist('plot_flag','var')
    plot_flag=0;
end

if ~exist('sdlim_red_fact','var')
    sdlim_red_fact=1;
end

if ~exist('lum_func','var')
    lum_func = 'log-norm';
end

msp_min_frot=60;                                                            % only consider MSPs as having rot freqs > this value
msp_min_fgw=2*msp_min_frot;                                                 % corresponding minimum GW freq

tbl=readtable('./data/pulsar_data_atnf_f0_fdots_dist_Bfield_type_age.csv',...    % read in f,fdot,Bext, age, distance, etc. from ATNF
    'NumHeaderLines',1);  
fs_dist=tbl.x_Hz_;
fdots_dist=tbl.x_s__2_;

fs_dist=2*fs_dist;                                                          % converts f rot to f gw
inds=fs_dist>=msp_min_fgw;                                                  % finds indicies of fgw > f gw MSP in ATNF catalog
UL_improvement_factor=1;
if strcmp(ellip_dist,'atnf')                                                % not used
%     sdlim_red_fact=10;
    epsilon=calc_eps_sd(fs_dist,fdots_dist,Izz)/sdlim_red_fact;
    epsilon=epsilon(inds);
    epsilon=epsilon(epsilon>0);
    log_ellip_dist=log10(epsilon);

    crustal_u=1e-2*(epsilon/1e-7);
    log_crust_dist=log10(crustal_u);
    [bandwidth_u,density_u,xmesh_u,cdf_u]=kde(log_crust_dist,2^14); %%% pdf on the log10(ellip)

    [N_u,edges_u]=histcounts(log_crust_dist,50,'Normalization','pdf');

elseif strcmp(ellip_dist,'exponential')                                     

    epsilon_sd=calc_eps_sd(fs_dist,fdots_dist,Izz);
    epsilon_sd=epsilon_sd(inds);
    epsilon_sd=epsilon_sd(epsilon_sd>0);
    
    %     eps_bar=5e-7; %arbitrary 
    eps_bar= 10^(mean(log10(epsilon_sd)));                                     %  from atnf
    eps_min= min(epsilon_sd);%5.38e-11;                                     % min value of epsilon from atnf
%     eps_min=1e-9;
%     eps_max=2.5e-6; %considers young pulsars too
    eps_max=max(epsilon_sd);%1.57e-5;%9.25e-7;                              % max value of epsilon from atnf
    R1=1-exp(-eps_min/eps_bar);
    R2=1-exp(-eps_max/eps_bar);
    Neps=1000;
    R=R1+rand(1,Neps)*(R2-R1);
    epsilon=-eps_bar*log(1-R);
    log_ellip_dist=log10(epsilon);

    crustal_u=1e-2*(epsilon/1e-7);
    log_crust_dist=log10(crustal_u);
    [bandwidth_u,density_u,xmesh_u,cdf_u]=kde(log_crust_dist,2^14); %%% pdf on the log10(ellip)

    [N_u,edges_u]=histcounts(log_crust_dist,50,'Normalization','pdf');
    figure; hold on; bar(edges_u(1:length(N_u)),N_u,'cyan'); plot(xmesh_u,density_u,'k','LineWidth',2);
    xlabel('log_{10} crustal strain'); ylabel('probability density  function'); set(gca,'FontSize',18)

%     UL_improvement_factor=100; %for ET, ~10x better sensitivity in \epsilon than O3
elseif strcmp(ellip_dist,'magnetic')
    
    B_surface_fields=tbl.x_G_(inds);                                        % external magnetic field of MSPs from ATNF, in Gauss
    B_surface_fields=B_surface_fields(~isnan(B_surface_fields));            % selects only those with measured B fields
    Bint=prop_factor.*B_surface_fields;                                     % calculates internal magnetic field
    
    epsilon=1e-8*Bint/1e12;                                                 % eq. 10, https://arxiv.org/abs/2007.14251

    log_ellip_dist=log10(epsilon);

elseif strcmp(ellip_dist,'crustal')

    epsilon_sd=calc_eps_sd(fs_dist,fdots_dist,Izz);
    epsilon_sd=epsilon_sd(inds);
    epsilon_sd=epsilon_sd(epsilon_sd>0);
    eps_min=min(epsilon_sd);
    eps_max=max(epsilon_sd);
    umin=10^5*eps_min;
    umax=10^5*eps_max;
%     umax=0.1;
%     umin=
    u=logspace(umin,umax);                                                  % 0.1 maximum given by https://arxiv.org/abs/0904.1986
    epsilon=1e-7*u/1e-2;                                                    % two orders of magnitude lower, MSPs is O(10^-4) 
                                                                            % from GW constraints on ellip MSP
    log_ellip_dist=log10(epsilon);
    UL_improvement_factor=1;
elseif strcmp(ellip_dist,'temperature')
    B_surface_fields=tbl.x_G_(inds); %magnetic field, in Gauss
    B_surface_fields=B_surface_fields(~isnan(B_surface_fields));
    Bint=prop_factor.*B_surface_fields;

    epsilon=1e-12*Bint/1e9;                                                 % superfluid cores, smaller ellipticites due to
    log_ellip_dist=log10(epsilon);                                          % temp asym, eq. 38 https://arxiv.org/pdf/1910.04453.pdf


end
log_f_dist=log10(fs_dist(inds));                                            % log10(frequencies) and log10(ellip) used for KDE


[bandwidth,density_el,xmesh_el,cdf]=kde(log_ellip_dist,2^14);               % pdf on the log10(ellip)

[N,edges]=histcounts(log_ellip_dist,50,'Normalization','pdf');

[bandwidth_f,density_f,xmesh_f,cdf_f]=kde(log_f_dist,2^14);                 % pdf on the log10(frequency)

[N_f,edges_f]=histcounts(log_f_dist,50,'Normalization','pdf');


if strcmp(search,'gcL')                                                     % load upper limits on h0 depending on which search
                                                                            % calculates TFFT since it affects sky resolution
    TT=readtable('~/Downloads/csv files/Recalibrated_UL_L_95_with_tffts.csv');
    fs_UL=TT.Var1;
    h0s_UL=TT.Var2;
    TFFTs= TT.Var3;
%     TT=readtable('~/Downloads/Recalibrated_UL_L_95_readable.txt');
elseif strcmp(search,'gcH')
    TT=readtable('~/Downloads/csv files/Recalibrated_UL_H_95_with_tffts.csv');
%     TT=readtable('~/Downloads/txt_files/Recalibrated_UL_H_95_readable.txt');
    fs_UL=TT.Var1;
    h0s_UL=TT.Var2;
    TFFTs= TT.Var3;
elseif strcmp(search,'O3')
    TT=readtable('./data/O3_GC_Upper_Limits_h095CL_LH.txt');

    fs_UL=TT.x_freq;
    h0s_UL=TT.h0_95;

    prop_constant=287146.905398613; %% Tfft=prop_const/sqrt(fmax)
    fmaxes=ceil(fs_UL/10)*10;
    TFFTs=rndeven(prop_constant./sqrt(fmaxes));
elseif strcmp(search,'O3binary')
    TT=readtable('~/Downloads/P2000298-v16-AncillaryMaterial/HB_95.dat');
    fs_UL=TT.Var1;
    h0s_UL=TT.Var2;
    TFFTs=1024*ones(size(fs_UL));

elseif strcmp(search,'O3allsky')
    fid=fopen('./data/O3_FH_UL.txt');
    A = textscan(fid,'%s %f64');
    fs_UL=A{1};

    if iscell(fs_UL)
        fs_UL=str2double(fs_UL);                                                % just a check, to ensure we get an array of frequencies
    end

    h0s_UL=A{2};
    conserv_factor = 1.06;

    h0s_UL = h0s_UL * conserv_factor;                                       % really this is frequency and sky-dependent

    fmax_db1=128; fmax_db2=512; fmax_db3=1024; 
    TFFTs(fs_UL<=fmax_db1)=8192;
    TFFTs(fs_UL<=fmax_db2 & fs_UL>fmax_db1)=4096;
    TFFTs(fs_UL>fmax_db2 & fs_UL<=fmax_db3)=2048;
    TFFTs(fs_UL>fmax_db3)=1024;

end

fs_UL=fs_UL(fs_UL>=msp_min_fgw);                                            % selects only ULs above MSP min f GW
h0s_UL=h0s_UL(fs_UL>=msp_min_fgw);
TFFTs=TFFTs(fs_UL>=msp_min_fgw);

ND=calc_ND( fs_UL,TFFTs);                                                   % computes ND, eq. 39 in arXiv:1407.8333

Nsky=4*pi*ND.^2;                                                            % computes # sky points, eq. 43 in arxiv:1407.8333 
sky_res_rad=2*pi./sqrt(Nsky);                                               % roughly sky resolution in radians
radius_of_patch=sky_res_rad*GC_dist*1000;                                   % sky resolution in pc

ellip_UL=calc_ellipticity(h0s_UL,fs_UL,GC_dist,Izz)/UL_improvement_factor; % converts h0 UL to ellipticity ULs



if strcmp(lum_func,'log-norm')
    lum_func_model=readtable('lumin.txt');                                        %.Var1: L_0 ; .Var2: \sigma_L ; .Var3: N_{MSP}
    Nmsp=lum_func_model.Var3;
elseif strcmp(lum_func,'a-gamma')
    lum_func_model=readtable('eta-ar.txt'); 
    Nmsp=10.^lum_func_model.Var3;
elseif strcmp(lum_func,'b-gamma')
    lum_func_model=readtable('eta-br.txt'); 
    Nmsp=10.^lum_func_model.Var3;
elseif strcmp(lum_func,'d-gamma')
    lum_func_model=readtable('eta-dr.txt'); 
    Nmsp=10.^lum_func_model.Var3;
end
 L0=lum_func_model.Var1;
 sigmaL=lum_func_model.Var2;


ellip_kde=10.^xmesh_el(xmesh_f>=log10(msp_min_fgw));                        % just a check to ensure ellipticites taken at valid fs
ellip_pdf=density_el(xmesh_f>=log10(msp_min_fgw));


f_kde=10.^xmesh_f(xmesh_f>=log10(msp_min_fgw));                             % just a check to ensure frequencies are w/in MSP range
f_pdf=density_f(xmesh_f>=log10(msp_min_fgw));



d_logellip=abs(xmesh_el(2)-xmesh_el(1));                                    % the integration steps
d_logf=abs(xmesh_f(2)-xmesh_f(1));

Pgw=GeV_excess_calc_P_GW(fs_UL,ellip_UL,f_kde,f_pdf,ellip_kde,ellip_pdf);

for ellip=1:length(ellip_UL)                                            % for each value of the ellipticity UL
    one_ellip=ellip_UL(ellip);
    inds_ellip=one_ellip<ellip_kde;                                     % when ellip in PDF is above ellip UL = detectable
    num_detectable_MSPs(:,ellip)=...                             % integrate over ellipticity distribution
        Nmsp * sum(ellip_pdf(inds_ellip)*d_logellip);              % calculates # detectable MSPs by LIGO if ellip PDF true
    % performed at each ellip UL (each freq) for each NMSP

end

ff = repmat(fs_UL',[size(Nmsp,1),1]);
ee = repmat(ellip_UL',[size(Nmsp,1),1]);

for r=1:length(fs_UL)                                                       % at each freq find index of nearest bin on f PDF 
    [~,index(r)]=min(abs(fs_UL(r)-f_kde)); %% 
end    

for ii=1:length(Nmsp)                                                       % for each NMSP that explains GeV excesss
    N_msps_int_over_f(ii)=...                                               % integrate over ATNF frequency distribution
        sum(num_detectable_MSPs(ii,:)'.*f_pdf(index)*d_logf); %.*Nmsp(ii)
end                                                                         % obtain # detectable MSPs int over freq and ellip PDFs
N_msps_int_over_f=N_msps_int_over_f';


if plot_flag>=1                                                             % plots detectable MSPs vs. freq and ellip UL
                                                                            % interpret: dirac delta PDF for freq, at each freq

    figure;scatter(L0(N_msps_int_over_f>1),sigmaL(N_msps_int_over_f>1),10,N_msps_int_over_f(N_msps_int_over_f>1)','o','filled'); colormap('parula'); colorbar; hold on;
    xlabel('log L_0 (erg/s)'); ylabel('\sigma_L'); cblabel('N_{GW}'); set(gca,'FontSize',18)
%     set(gca,'xscale','log') ;
    set(gca,'ColorScale','log')

    figure;scatter(ff(num_detectable_MSPs>=1),num_detectable_MSPs(num_detectable_MSPs>=1),2,ee(num_detectable_MSPs>=1),...
        'o','filled'); colormap('parula'); colorbar; hold on;
    xlabel('frequency (Hz)'); ylabel('number of detectable MSPs'); cblabel('ellipticity upper limit'); set(gca,'FontSize',18)
%     set(gca,'xscale','log') ;
    set(gca,'yscale','log'); 
    set(gca,'ColorScale','log');
end

if plot_flag>2                                                              % plots ellip PDF
    figure; hold on; bar(edges(1:length(N)),N,'cyan'); plot(log10(ellip_kde),ellip_pdf,'k','LineWidth',2);
    xlabel('log_{10} ellipticity'); ylabel('probability density  function'); set(gca,'FontSize',18)

    figure; bar(edges_f(1:length(N_f)),N_f,'blue');                         % plots frequency PDF
    hold on; plot(log10(f_kde),f_pdf,'k','LineWidth',2);
    xlabel('log_{10} frequency (Hz) '); ylabel('probability density  function'); set(gca,'FontSize',18)



    figure;semilogy(fs_UL,ellip_UL)                                         % plots ellip vs freq UL values
    xlabel('frequency (Hz)'); ylabel('ellipticity'); set(gca,'FontSize',14)

end


if ~strcmp(ellip_dist,'atnf') 
    log_crust_dist=[]; xmesh_u=[]; density_u=[];
end
end