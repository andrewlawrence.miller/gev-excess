function [Pgw_mag,Pgw_sds,mag_prop_fact,sd_lim_fact,Izzs] = run_run_GeV_excess_calc_P_GW
%run_run_GeV_excess_calc_P_GW() This function calculates the probability of
%detecting a gravitational wave from an isolated neutron star at the
%galactic center using upper limits from O3. It assumes that the population
%of undetected millilsecond pulsars have ellipticities and frequencies
%distributed according to the ATNF catalog, that the moments of inertia are
%[1,5,10]e38, and that an internal magnetic field is responsible for the
%neutron star spin down, OR simply that a fraction of rotational energy
%goes into GWs (model agnostic).

%   -----------------------------INPUTS:-----------------------------------
%   None - for more flexability, call run_GeV_excess_calc_P_GW

%   -----------------------------OUTPUTS:----------------------------------

%       Pgw_mag:    probability of detecting a GW from a MSP in O3 if
%       spin-down driven by internal magnetic field
%       Pgw_sds:    probability of detecting a GW from a MSP in O3 if we
%       assume that a fraction of the rotational energy is emitted via GWs
%       mag_prop_fact: the array of proportionality factors that dictates
%       the strength of the internal magentic field (same size as Pgw_mag)
%       sd_lim_facts: 1/sdsfacts^2 is the fraction of energy emitted via
%       GWs (same size as Pgw_sds)



%   Detailed explanation goes here
warning('off','all')
mag_prop_fact=logspace(0,4,200);%[1 5 10 50 100 500 1000 5000 10000];%

sd_lim_fact=logspace(0,2);%[1 5 10:10:100];

% Izzs=(1:10)*1e38;%logspace(38,39);
Izzs=[1 5 10]*1e38;

for magfact = 1:length(mag_prop_fact)
    for Izz=1:length(Izzs)
        Pgw_mag(magfact,Izz)=run_GeV_excess_calc_P_GW('magnetic',mag_prop_fact(magfact),Izzs(Izz),'O3allsky',8);
    end
    fclose('all');
end
[X,Y]=meshgrid(Izzs,mag_prop_fact);
% figure;scatter(X(:),Y(:),10,Pgw_mag(:),'o','filled'); colorbar
% set(gca,'ColorScale','log'); set(gca,'YScale','log')


% figure;loglog(mag_prop_fact,Pgw_mag(:,1)) %1e38 for Izzs=(1:10)*1e38;
% hold on; loglog(mag_prop_fact,Pgw_mag(:,5)) %5e38 for Izzs=(1:10)*1e38;
% hold on; loglog(mag_prop_fact,Pgw_mag(:,10)) %1e39 for Izzs=(1:10)*1e38;
% xlabel()


for sdfact = 1:length(sd_lim_fact)
    for Izz=1:length(Izzs)
        Pgw_sds(sdfact,Izz)=run_GeV_excess_calc_P_GW('atnf',sd_lim_fact(sdfact),Izzs(Izz),'O3allsky',8);
    end
    fclose('all');
end
% 
% figure;loglog(sd_lim_fact,Pgw_sds(:,1))
% hold on;loglog(sd_lim_fact,Pgw_sds(:,5))
% hold on;loglog(sd_lim_fact,Pgw_sds(:,10))

end