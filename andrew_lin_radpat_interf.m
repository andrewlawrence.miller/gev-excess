function [Fpluss,Fcross,F_sq_avg_over_psi]=andrew_lin_radpat_interf(alpha,del,antenna,tsid,psi)
%LIN_RADPAT_INTERF  interferometric detector linear response to a source
%                      (only linear polarization)
%
%   antenna:     antenna structure
%           .lat    longitude (deg)
%           .long   latitude (deg)
%           .azim   azimuth (deg) (detector x-arm, from south to west)
%           .incl   inclination (deg) (to be implemented)
%
%   source      source structure
%           .a      right ascension (degree)
%           .d:     declination (degree)
%           .psi    linear polarization angle
%           .eps    percentage of linear polarization
%
%   tsid       sidereal time (hour)
%

% New version May 2009, C. Palomba
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if ~exist('psi','var')
    psi=0;
end

alpha=alpha*pi/180;
del=del*pi/180;
%eps=source.eps;
psi=psi*pi/180;
a=antenna.azim*pi/180;
lam=antenna.lat*pi/180;
long=antenna.long*pi/180;
TH=tsid/12*pi;

a0=-(3/16)*(1+cos(2*del))*(1+cos(2*lam))*cos(2*a);
a1c=-(1/4)*sin(2*del)*sin(2*lam)*cos(2*a);
a1s=(1/2)*sin(2*del)*cos(lam)*sin(2*a);
a2c=-(1/16)*(3-cos(2*del))*(3-cos(2*lam))*cos(2*a);
a2s=(1/4)*(3-cos(2*del))*sin(lam)*sin(2*a);

b1c=-cos(del)*cos(lam)*sin(2*a);
b1s=-(1/2)*cos(del)*sin(2*lam)*cos(2*a);
b2c=-sin(del)*sin(lam)*sin(2*a);
b2s=-(1/4)*sin(del)*(3-cos(2*lam))*cos(2*a);

f1=a0+a1c*cos(alpha-TH)+a1s*sin(alpha-TH)+a2c*cos(2*alpha-2*TH)+a2s*sin(2*alpha-2*TH);
f2=b1c*cos(alpha-TH)+b1s*sin(alpha-TH)+b2c*cos(2*alpha-2*TH)+b2s*sin(2*alpha-2*TH);
Fpluss=f1*cos(2*psi)+f2*sin(2*psi);
Fcross=f2*cos(2*psi)-f1*sin(2*psi);


F_sq_avg_over_psi = 1/2 * (f1.^2 + f2.^2); % identical for fplus and fcross




