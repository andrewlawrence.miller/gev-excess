function [P_GW] = GeV_excess_calc_P_GW(fs_UL,ellip_UL, ...
    f_kde,f_pdf,ellip_kde,ellip_pdf)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

f_kde_log10=log10(f_kde);
ellip_kde_log10=log10(ellip_kde);

d_logellip=abs(ellip_kde_log10(2)-ellip_kde_log10(1));                                    % the integration steps
d_logf=abs(f_kde_log10(2)-f_kde_log10(1));

for ellip=1:length(ellip_UL)                                                % for each value of the ellipticity UL
    one_ellip=ellip_UL(ellip);
    inds_ellip=one_ellip<ellip_kde;                                         % when ellip in PDF is above ellip UL = detectable
    pgw_ellip(ellip)=...                                                    % integrate over ellipticity distribution
        sum(ellip_pdf(inds_ellip)*d_logellip);                              % calculates # detectable MSPs by LIGO if ellip PDF true
                                                                            % performed at each ellip UL (each freq) for each NMSP

end


for r=1:length(fs_UL)                                                       % at each freq find index of nearest bin on f PDF 
    [~,index(r)]=min(abs(fs_UL(r)-f_kde)); %% 
end    

P_GW=(pgw_ellip*f_pdf(index)*d_logf);                                       % this works b/c (1 x N) * (N x 1) = 1 x 1 (it's a sum)

end