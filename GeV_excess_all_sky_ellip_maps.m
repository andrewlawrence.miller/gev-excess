function [C_a_d_psi_iota,C_a_d] = GeV_excess_all_sky_ellip_maps()
%GeV_excess_all_sky_ellip_maps 
% This function computes the factors that population-averaged upper
% limits from the Frequency-Hough all-sky search pipeline need to be
% multiplied by to obtain source-specific upper limits, that is, upper
% limits on h0 or ellipticity for a source with a particular sky location,
% inclination angle and polarization angle.

% The input to this function are the h0(f) population-averaged upper
% limits, where "population-averaged" means that they have been computed
% with a sensitivity formula that averages over time, sky position,
% polarization angle and inclination angle. The goal of this function is to
% remove those averages to obtain an upper limit for a particular source.

% This function also computes the same factor, but for the case where the
% source-specific parameters are only its sky position, that is,
% inclination angle and polarization angle are averaged out.

%   -----------------------------OUTPUTS:----------------------------------
% 
%   C_a_d_psi_iota:     scaling factor to multiply pop-avg UL by to specify
%                       it to a specific source with sky position, 
%                       inclination angle and polarization angle
%
%   C_a_d               scaling factor to multiply pop-avg UL by to specify
%                       it to a specific source with sky position, though
%                       inclination and polarization angles avg over

dist = 1;                                                                   % kpc
Izz = 10^38;                                                                % kg m^2


% load population- average upper limits
fid=fopen('/Users/andrewmiller/Desktop/Belgio/O3_all-sky/o3-all-sky-pbh-constraints/limits/O3_FH_UL.txt');
A = textscan(fid,'%s %f64');
fs_UL=A{1};
h0s_UL=A{2};
fs_UL=str2double(fs_UL);

[~,a]=min(fs_UL);
[~,b]=max(fs_UL);

% fs_UL=[fs_UL(a) fs_UL(b)];                                                  % just for tests, restrict to f=20 and f=2000 Hz
fs_UL=fs_UL(b);
% h0s_UL = [h0s_UL(a) h0s_UL(b)];
h0s_UL=h0s_UL(b);

fmax_db1=128; fmax_db2=512; fmax_db3=1024;                                  
                                                                            % FFT length changes depending on frequency
tffts(fs_UL<=fmax_db1)=8192;                                                % up to 128 Hz, 8192 s
tffts(fs_UL<=fmax_db2 & fs_UL>fmax_db1)=4096;                               % between 128 and 512 Hz, 4096 s
tffts(fs_UL>fmax_db2 & fs_UL<=fmax_db3)=2048;                               % between 512 and 1024 Hz, 2048 s
tffts(fs_UL>fmax_db3)=1024;                                                 % above 1024 Hz, 1024 s
tffts=tffts';                                                               % This is done b/c # of sky points scales w/ (TFFT*f)^2
                                                                            % --> can't afford high TFFT at high freq

ND = calc_ND(fs_UL,tffts');                                                 % calculate ND   
antenna=ligol;                                                              % use LIGO Livingston parms to calculate F+ and Fx
% dt=0.1;                                                                     % time step (hours) over which to calculate F+ and Fx
% tsid=0:dt:24-dt;                                                            % calc F+ and Fx over 1 sidereal day w/ step dt (hours)


S_sq_all_avg=4/25;                                                          % factor by which all-sky ULs have been scaled by
                                                                            % all-sky ULs are population avg- this factor comes
                                                                            % from averaging over sky, polariz, iota
                                                                            % eq. A4, DCC:P2100367

for freq=1:length(fs_UL)                                                    % for each frequency, compute the sky-map
    [x,b,index,nlon] = pss_optmap(ND(freq));                                % computes the lons and lats that are searched over
    lons = x(:,1);
    lats = x(:,2);                                                          % these are arrays with all possible combos of lon/lat
    [alphas,deltas]=astro_coord('ecl','equ',lons,lats); 
    f = fs_UL(freq);
    h0 = h0s_UL(freq);
%     for pos = 1:length(lons)                                                % for each sky position
%         lon = lons(pos);
%         lat = lats(pos);
%         iota= pi/4;                                                         % iota/psi fixed as an example
        psi=30;
        cosiota=cos(pi/4);
% 
% %         psi = 0 ; % can go from 0 to pi , https://dcc.ligo.org/public/0106/T1300666/003/Whelan_geometry.pdf
%         [fpluss,fcross,f_psi_avg] = ...                                     % computes F+,Fx and <F+>_\psi=<Fx>_\psi
%             andrew_lin_radpat_interf(lon,lat,antenna,tsid,psi);
%                
% %         [fpluss,fcross,f_psi_avg]=calc_fplus_fcross(lon,lat,psi);
% 
%         fplus_sq_time_avg=sum(fpluss.^2*dt)/(tsid(end)-tsid(1));            % computes time averages of F+ and Fx
%         fcros_sq_time_avg=sum(fcross.^2*dt)/(tsid(end)-tsid(1));            % they are equal
%                                                                             % for these lines,
%                                                                             % see eq. 9-12, https://inspirehep.net/literature/874881
%         Apluss_sq = ( (1 + cos(iota)^2) / 2 ) ^2;                           % computes quantities A+ and Ax
%         Across_sq = cos(iota)^2;
% 
% 
%         S_t_sq=fplus_sq_time_avg * Apluss_sq...                             %  eq. A5, DCC:P2100367
%             + fcros_sq_time_avg * Across_sq;
% 
%         C_a_d_psi_iota{freq}(pos) = sqrt ( S_sq_all_avg / S_t_sq );         % eq. A6,  DCC:P2100367
%                                                                             % specific to alpha, delta, psi, iota
% 
%         h0_corrected_a_d_psi_iota{freq}(pos) = ...                          % computes h0 UL for specific source
%             h0 * C_a_d_psi_iota{freq}(pos);
%         ellip_corrected_a_d_psi_iota{freq}(pos)...                          % computes ellip UL for specific source
%             =calc_ellipticity(h0_corrected_a_d_psi_iota{freq}(pos),f,dist,Izz);

        %%%% above is for a specific source, with chosen alpha, delta, psi
        %%%% and iota. Below, we wish to integrate over psi and iota, but
        %%%% allow alpha and delta to vary




%         f_psi_t_avg=sum(f_psi_avg) / (tsid(end)-tsid(1));                % F+ and Fx are identical when average over psi and t
%         iota_avg_fact = 4/5;                                                % int_{-1}^{1} A+^2+Ax^2  d(cos iota) = 4/5
                                                                            % eq. B17, arXiv:1407.8333

%         S_t_psi_sq= f_psi_t_avg * iota_avg_fact;                           % use eq. A5, DCC:P2100367 : since <F+^2>=<Fx^2>, 
                                                                           % factor out that term, avg over A+^2+Ax^2, compute S
                                                                            
        [~,~,~,S_t_psi_iota_sq]=detector_response_edited...
            (h0,deltas',cosiota,psi,antenna);
        C_a_d{freq}= sqrt( S_sq_all_avg ./ S_t_psi_iota_sq);                 % compute "new" scaling factor that deps only on sky pos

        h0_corrected_a_d{freq} = h0 * C_a_d{freq};                          % computed scaled h0 and ellip UL
        ellip_corrected_a_d{freq}...                                        % for diff sky positions at fixed freq
            =calc_ellipticity(h0_corrected_a_d{freq},f,dist,Izz);

        

%     end
    figure;scatter(alphas,deltas,25,C_a_d{freq}','o','filled');             % plots scatter of skymap with scaling factor colored
    colormap('parula'); colorbar
    matrix=([alphas deltas C_a_d{freq}' ellip_corrected_a_d{freq}']);
    T=array2table(matrix);  
    writetable(T,'./data/skymap.csv')                                       % write lons, lats and scaling factor to csv file
end

end