function [Fplus,Fcros,F_sq_avg_over_psi] = calc_fplus_fcross(alpha,delta,psi,phi,t,antenna)

omega_r=2*pi/86400 * 3600; % in rad/hour

if ~exist('phi','var')
    phi=0;
end

if ~exist('antenna','var')
    antenna=livingston;
end

if ~exist('t','var')
    dt=0.1;
    t=0:dt:24-0.1;
end

alpha=alpha/180.*pi;
delta=delta/180.*pi;
psi=psi/180.*pi;



azim=antenna.azim/180.*pi;
lat=antenna.lat/180.*pi;
long=antenna.long/180.*pi;

gamma = antenna.gamma / 180 * pi;
lambda = antenna.lambda / 180 * pi;
zi = antenna.zi / 180 * pi;



cos2gam = cos(2 * gamma);
sin2gam = sin(2 * gamma);

cos2lam = cos(2 * lambda);
sin2lam = sin(2 * lambda);

cos2del = cos(2 * delta);
sin2del = sin(2 * delta);

time_2piece = 2 * (alpha - phi - omega_r * t);

cos_2time = cos(time_2piece);
sin_2time = sin(time_2piece);

term1 = 1/16 * sin2gam * (3 - cos2lam) * (3 - cos2del) * cos_2time;

sinlam = sin(lambda);

term2 = -1/4 * cos2gam * sinlam * (3-cos2del) * sin_2time;

time_piece = alpha - phi - omega_r * t;

cos_time = cos(time_piece);

term3 = 1/4 * sin2gam * sin2del * cos_time;

coslam = cos(lambda);

sin_time = sin(time_piece);

term4 = -1/2 * cos2gam * coslam * sin2del * sin_time;

cosdel = cos(delta);

term5 = 3/4 * sin2gam * coslam^2 * cosdel^2;

a_of_t = term1 + term2 + term3 + term4 + term5;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sindel = sin(delta);

term6 = cos2gam * sinlam * sindel * cos_2time;

term7 = 1/4 * sin2gam * (3-cos2lam) * sindel * sin_2time;

term8 = cos2gam * coslam * cosdel * cos_time;

term9 = 1/2 * sin2gam * sin2lam * cosdel * sin_time;


b_of_t = term6 + term7 + term8 + term9;

sinzi = sin(zi);

cos2psi = cos(2 * psi);
sin2psi = sin(2 * psi);

Fplus = sinzi * ( a_of_t * cos2psi + b_of_t * sin2psi );
Fcros = sinzi * ( b_of_t * cos2psi - a_of_t * sin2psi );

dt=t(2)-t(1);

% Fplus_sq_time_avg=sum(Fplus.^2*dt)/(t(end)-t(1)+dt);
% Fcros_sq_time_avg=sum(Fcros.^2*dt)/(t(end)-t(1)+dt);

F_sq_avg_over_psi = 1/2 * (a_of_t.^2 + b_of_t.^2); % identical for fplus and fcross

end

function ant=livingston()

ant.name='ligol';
ant.lat=30.563;
ant.lambda=ant.lat;
ant.L=90.77;
ant.gamma=243;
ant.zi=90;

ant.long=269.266;
ant.azim=72.2836;
ant.height=-6.5;
ant.whour=-6;
ant.shour=-5;
ant.incl=0;
ant.type=2;

end