function [Pgw_plots_mag,Pgw_plots_sds,Bfact,Izzs,sd_facts] = Gev_excess_Pgws_for_excl_plots()
%UNTITLED3 Summary of th
% 

Bfact=[150 150];
Izzs=[1e38 5e38];
sd_facts = [1/0.07 1/0.1];

warning('off','all')
[Pgw_plots_mag(1)] = run_GeV_excess_calc_P_GW('magnetic',Bfact(1),Izzs(1),'O3allsky',8);
[Pgw_plots_mag(2)] = run_GeV_excess_calc_P_GW('magnetic',Bfact(1),Izzs(2),'O3allsky',8);
[Pgw_plots_sds(1)] = run_GeV_excess_calc_P_GW('atnf',sd_facts(1),Izzs(1),'O3allsky',8); % 7%
[Pgw_plots_sds(2)] = run_GeV_excess_calc_P_GW('atnf',sd_facts(2),Izzs(1),'O3allsky',8); % 10%ie
end