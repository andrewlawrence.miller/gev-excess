function [C_a_d_psi_iota] = calc_UL_source_spec_scale_factor(lon,lat,psi,iota,tsid,antenna)
%calc_UL_source_spec_scale_factor 
% This function computes the source-specific scaling factor by which to
% multiply population-averaged upper limits.

%   -----------------------------INPUTS:-----------------------------------
%   lon:            source longitude (degrees)
%   lat:            source latitude (degrees)
%   psi:            source polarization angle [0,90] (degrees)
%   iota:           inclination angle (radians)
%   tsid:           time at which you would like to eval F+,Fx (hours)
%   antenna:        which detector (def: ligol)
%
%   -----------------------------OUTPUTS:----------------------------------
% 
%   C_a_d_psi_iota: scaling factor to multiply pop-avg UL by


%   Detailed explanation goes here
S_sq_all_avg=4/25;

if ~exist('antenna','var')
    antenna=ligol;
end

if ~exist('tsid','var')
    dt=1;
    tsid=0:dt:86400-dt;
    tsid=tsid/3600; % 3600 s in every hour
    dt=dt/3600;
end

% [Fpluss,Fcross] = andrew_lin_radpat_interf(lon,lat,antenna,tsid,psi);
[Fpluss,Fcross] = calc_fplus_fcross(lon,lat,psi,0,tsid);

fplus_sq_time_avg=sum(Fpluss.^2*dt)/(tsid(end)-tsid(1)+dt);
fcros_sq_time_avg=sum(Fcross.^2*dt)/(tsid(end)-tsid(1)+dt);

Apluss_sq = ( (1 + cos(iota)^2) / 2 ) ^2;
Across_sq = cos(iota)^2;

S_t_sq=fplus_sq_time_avg * Apluss_sq + fcros_sq_time_avg * Across_sq;

C_a_d_psi_iota = sqrt ( S_sq_all_avg / S_t_sq ); % specific to alpha, delta, psi, iota

end