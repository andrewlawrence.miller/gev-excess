function epsilon=calc_eps_sd(freqs,fdots,I)

%%% equating equations 4 and 5 in https://arxiv.org/pdf/2012.12926.pdf and
%%% solving for ellipticity \epsilon

if ~exist('I','var')
    I=10^38; %kg *m^2
end

prefact=(1/(16*pi^2))*sqrt(5/2);

consts=constants;
c=consts.c;
G=consts.G;

factor=sqrt(c^5.*abs(fdots)./(G*I.*freqs.^5));

epsilon=prefact*factor;



end