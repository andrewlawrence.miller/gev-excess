function [Fplus2ave, Fcross2ave, factor,factor_avg_over_psi_iota] = detector_response_edited(hul,delta,cosiota,psi,ant)

% simple code to compute UL correction from average value to the value
% specific for a given set of signal parameters
%
% Input arguments:
%   hul:    average UL
%   delta:  source declination (degree, equatorial)
%   cosiota:source cosine of the inclination angle
%   psi:    source polarization angle (degree)
%   ant:    detector structure (ligoh, ligol, virgo)
%
% Usage example: 
% [Fplus2ave, Fcross2ave, factor] =detector_response(1.3e-25,-33.436671008244581,-0.080665933938189,25.455368922072214,ligoh);
% C. Palomba and the Rome Virgo group (May 2022)

% detector parameters (re-defined to be compliant with the definitions in JKS)
lat=ant.lat;
long=360-ant.long;
azim=360-ant.azim-45;

%%%%%%%%%%%
% out=rand_point_on_sphere(40000);
% delta = out(:,2);
% delta = delta';
% %delta=-90+180*rand(1,10000);
% psi=-45+90*rand(1,40000);
%%%%%%%%%%%%%%%%%%%%%%%%%

% if you want to use specific input values COMMENT these lines
%delta = -90 + 180*rand(1,40000);
%psi = -45 + 90*rand(1,40000);
%cosiota = -1 +2*rand(1,40000);
% --> check with average cosiota and delta,psi random

% convert angle to radians
delta = delta./180*pi;
psi = psi/180*pi;

lat = lat/180*pi;
long = long/180*pi;
azim = azim/180*pi;

% Functions a^2(t) and b^2(t) averaged over time
a2t = 1/512*sin(2*azim)^2*(3-cos(2*lat))^2*(3-cos(2*delta)).^2 + 1/32*cos(2*azim)^2*sin(lat)^2*...
    (3-cos(2*delta)).^2 + 1/32*sin(2*azim)^2*sin(2*lat)^2*sin(2*delta).^2 + 1/8*cos(2*azim)^2*cos(lat)^2*sin(2*delta).^2+...
    9/16*sin(2*azim)^2*cos(lat)^4*cos(delta).^4;

b2t = 1/2*cos(2*azim)^2*sin(lat)^2*sin(delta).^2 + 1/32*sin(2*azim)^2*(3-cos(2*lat))^2*sin(delta).^2+...
    1/2*cos(2*azim)^2*cos(lat)^2*cos(delta).^2 + 1/8*sin(2*azim)^2*sin(2*lat)^2*cos(delta).^2;

Fplus2t = (a2t.*cos(2*psi).^2 + b2t.*sin(2*psi).^2);
Fcross2t = (a2t.*sin(2*psi).^2 + b2t.*cos(2*psi).^2);

factor_specific = (Fplus2t.*((1+cosiota.^2)/2).^2 + Fcross2t.*cosiota.^2);

% compute average factor. In fact analytic computation gives factor=0.4. Here the
% numerical computation is done to easily allow to implement different choices, 
% like averages on some specific parameters.

% choose random values of the parameters
% npoint = 40000;
npoint=length(delta);
out=rand_point_on_sphere(npoint);
% delta = out(:,2);
% delta = delta';
%delta=-90+180*rand(1,10000);
psi=-45+90*rand(1,npoint);
cosiota=-1+2*rand(1,npoint);

% delta = delta/180*pi;
psi = psi/180*pi;

% functions a^2(t), b^2(t) averaged over alpha (source right ascension)
a2ave = 1/512*sin(2*azim)^2*(3-cos(2*lat))^2*(3-cos(2*delta)).^2 + 1/32*cos(2*azim)^2*sin(lat)^2*...
    (3-cos(2*delta)).^2 + 1/32*sin(2*azim)^2*sin(2*lat)^2*sin(2*delta).^2 + 1/8*cos(2*azim)^2*cos(lat)^2*sin(2*delta).^2+...
    9/16*sin(2*azim)^2*cos(lat)^4*cos(delta).^4;

b2ave = 1/2*cos(2*azim)^2*sin(lat)^2*sin(delta).^2 + 1/32*sin(2*azim)^2*(3-cos(2*lat))^2*sin(delta).^2+...
    1/2*cos(2*azim)^2*cos(lat)^2*cos(delta).^2 + 1/8*sin(2*azim)^2*sin(2*lat)^2*cos(delta).^2;

Fplus2ave = (a2ave.*cos(2*psi).^2 + b2ave.*sin(2*psi).^2);

avg_over_psi_fplus2=a2ave.*mean(cos(2*psi).^2) + b2ave.*mean(sin(2*psi).^2);

Fcross2ave = (a2ave.*sin(2*psi).^2 + b2ave.*cos(2*psi).^2);
avg_over_psi_fcross2 = a2ave.*mean(sin(2*psi).^2) + b2ave.*mean(cos(2*psi).^2);

factor = (Fplus2ave.*((1+cosiota.^2)/2).^2 + Fcross2ave.*cosiota.^2);

factor_avg_over_psi_iota=avg_over_psi_fplus2.*mean(((1+cosiota.^2)/2).^2)+avg_over_psi_fcross2.*mean(cosiota.^2);

factor_avg_over_psi_iota_dec=mean(factor_avg_over_psi_iota); %equals mean(factor), as in average over all paramaters - just a check.

% mean_root_factor = sqrt(mean(factor));
% ratio = (mean(factor)./factor_specific);
% ratio = 1./ratio;
% ratio_mean = mean(ratio)
% ratio_50 = sqrt(prctile(ratio,50))
% ratio_95 = sqrt(prctile(ratio,95))
% ratio_05 = sqrt(prctile(ratio,5))
% [val bin]=hist(ratio,100);
% cu = cumsum(val)/sum(val);
% % figure;plot(bin,cu);
% % figure;hist(ratio,100);
% 
% mean_ratio_squared = mean(factor./factor_specific); % "correction" factor squared
% std_ratio_squared = std(factor/factor_specific);
% hul_corrected_min = hul*sqrt(mean_ratio_squared-std_ratio_squared);
% hul_corrected_max = hul*sqrt(mean_ratio_squared+std_ratio_squared);
% 
% hul_corrected = hul*sqrt(mean_ratio_squared); %"corrected" UL 
%fprintf('mean_ratio_squared=%f mean_root_factor=%f factor_specific=%f\n',mean_ratio_squared,mean_root_factor,factor_specific);
% fprintf('hul_corrected=%e hul_corrected_min=%e hul_corrected_max=%e\n',hul_corrected,hul_corrected_min,hul_corrected_max);

%%%%%%%%%%%
% mean_inverse_ratio_factor = sqrt(mean(factor_specific./factor));
% fprintf('factor to go from specific to average: %f\n',mean_inverse_ratio_factor);
