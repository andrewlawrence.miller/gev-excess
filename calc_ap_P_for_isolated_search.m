function [all_fs,all_Ps,ap,atnf_P,atnf_ap] = calc_ap_P_for_isolated_search()
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
% TFFT in s; freq in Hz

TT=readtable('./data/O3_GC_Upper_Limits_h095CL_LH.txt');

fs_UL=TT.x_freq;
% h0s_UL=TT.h0_95;

prop_constant=287146.905398613; %% Tfft=prop_const/sqrt(fmax)
fmaxes=ceil(fs_UL/10)*10;
TFFTs=rndeven(prop_constant./sqrt(fmaxes));
P=logspace(-1,2,1000);% days
[all_fs,all_Ps]=meshgrid(fs_UL,P);

fmax_db1=128; fmax_db2=512; fmax_db3=1024; 

tffts_interp(fs_UL<=fmax_db1)=8192;
tffts_interp(fs_UL<=fmax_db2 & fs_UL>fmax_db1)=4096;
tffts_interp(fs_UL>fmax_db2 & fs_UL<=fmax_db3)=2048;
tffts_interp(fs_UL>fmax_db3)=1024;
tffts_interp=tffts_interp';


for pp=1:length(P)

    ap(pp,:) = 0.076 * (P(pp) / 1) * (fs_UL / 100).^(-1) .* (TFFTs / 1800).^(-1);
end

% fact=0.0294100;                                                            % sum(fs_UL.^(-1).*(f_pdf(index)*d_logf))

all_fs=all_fs(:);
all_Ps=all_Ps(:);
ap=ap(:);

% ap_min=min(ap);
% ap_max=max(ap);
% P_min=min(P);
% P_max=max(P);


% for ff=1:length(freq)
%     for tt=1:length(TFFT)
% 
%         ap(ff,tt) = 0.076 * (P / 1) * (freq(ff) / 100).^(-1) * (TFFT(tt) / 1800).^(-1);
%     end
% end

tbl=readtable('./data/atnf_binary_parms.csv',...    % read in ap and P from ATNF catalog
    'NumHeaderLines',1);  
    
aps_atfn=tbl.x_ltsec_;
Ps_atfn=tbl.x_days_;
fs=tbl.x_Hz_;


atnf_ap=aps_atfn(aps_atfn~=-999 & Ps_atfn~=-999 & fs>60);
atnf_P=Ps_atfn(aps_atfn~=-999 & Ps_atfn~=-999 & fs>60);

tot_msps=sum(fs>60)
n_msps_isolated=sum(aps_atfn==-999 & fs>60)
n_msps_binaries=sum(aps_atfn~=-999 & fs>60)

figure;loglog(all_Ps,ap)
hold on; plot(atnf_P,atnf_ap,'sq')
% tot=sum(atnf_P>P_min & atnf_P<P_max & atnf_ap>ap_min & atnf_ap<ap_max);

end