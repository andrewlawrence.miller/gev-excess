This is a repository that contains the codes for generating the results present in the paper:

https://arxiv.org/abs/2301.10239

These Matlab codes are meant for users to constrain their own luminosity function, given the upper limits from a continuous gravitational-wave all-sky search.

To do this, users may open the Python Jupyter notebook "run_GeV_excess_constrain.ipynb" at: 

https://git.ligo.org/andrewlawrence.miller/gev-excess/-/blob/main/run_GeV_excess_constrain.ipynb



