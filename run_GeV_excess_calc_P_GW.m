function [Pgw] = run_GeV_excess_calc_P_GW(ellip_dist,mag_prop_fact_or_sd_red_fact,Izz,search,GC_dist)
%run_GeV_excess_calc_P_GW This function calculated the probability that a
%GW would have been detected in a particular observing run at a particular
%distance from us, if NSs were emitted GWs via a defined mechanism with a
%fixed moment of inertia

%   -----------------------------INPUTS:-----------------------------------
%   ellip_dist:         the ellipticity distribution you wish to assume for MSPs
%                       -currently, this is either 'magnetic' or 'atnf' or 'temperature'
%
%   mag_prop_fact_or_sd_red_fact:   this is the factor by which the internal
%                                   magnetic field is stronger than the external one (always > 1), OR the
%                                   a number that dictates what fraction of the spin-down limit you believe
%                                   GWs are being emited at. E.g. if 10, then h0_emited =h0_sd_lim / 10
%
%   Izz                 moment of inertia (kg * m^2)
%
%   search:             which upper limits to use ('O3' or 'O3allsky'
%                       implemented- the paper uses 'O3allsky', so recommend use those
%
%   GC_dist:            distance to the galactic center, kpc. We use 8 kpc
%   -----------------------------OUTPUTS:----------------------------------

%       Pgw_mag:    probability of detecting a GW from a MSP

if ~exist('ellip_dist','var')
    ellip_dist='magnetic';
end

if ~exist('search','var')
    search='O3';
end

if ~exist('GC_dist','var')
    GC_dist=8;%kpc
end

if ~exist('mag_prop_fact_or_sd_red_fact','var')                            % proportionality factor b/t internal B and surface B
    mag_prop_fact_or_sd_red_fact=1;
end

if ~exist('Izz','var')
    Izz=10^38;                                                              % fiducial value, 1.4Msun, 10 km rad star for I=2/5*MR^2
end

msp_min_frot=60;                                                            % only consider MSPs as having rot freqs > this value
msp_min_fgw=2*msp_min_frot;                                                 % corresponding minimum GW freq

tbl=readtable('./data/pulsar_data_atnf_f0_fdots_dist_Bfield_type_age.csv',...    % read in f,fdot,Bext, age, distance, etc. from ATNF
    'NumHeaderLines',1);  
fs_dist=tbl.x_Hz_;
fdots_dist=tbl.x_s__2_;

fs_dist=2*fs_dist;                                                          % converts f rot to f gw
inds=fs_dist>=msp_min_fgw;                                                  % finds indicies of fgw > f gw MSP in ATNF catalog
UL_improvement_factor=1;
if strcmp(ellip_dist,'atnf')                                                % not used
    epsilon=calc_eps_sd(fs_dist,fdots_dist,Izz)/mag_prop_fact_or_sd_red_fact;
    epsilon=epsilon(inds);
    epsilon=epsilon(epsilon>0);
    log_ellip_dist=log10(epsilon);

elseif strcmp(ellip_dist,'magnetic')
    
    B_surface_fields=tbl.x_G_(inds);                                        % external magnetic field of MSPs from ATNF, in Gauss
    B_surface_fields=B_surface_fields(~isnan(B_surface_fields));            % selects only those with measured B fields
    Bint=mag_prop_fact_or_sd_red_fact.*B_surface_fields;                                     % calculates internal magnetic field
    
    epsilon=1e-8*Bint/1e12;                                                 % eq. 10, https://arxiv.org/abs/2007.14251

    log_ellip_dist=log10(epsilon);


elseif strcmp(ellip_dist,'temperature')
    B_surface_fields=tbl.x_G_(inds); %magnetic field, in Gauss
    B_surface_fields=B_surface_fields(~isnan(B_surface_fields));
    Bint=mag_prop_fact_or_sd_red_fact.*B_surface_fields;

    epsilon=1e-12*Bint/1e9;                                                 % superfluid cores, smaller ellipticites due to
    log_ellip_dist=log10(epsilon);                                          % temp asym, eq. 38 https://arxiv.org/pdf/1910.04453.pdf


end
log_f_dist=log10(fs_dist(inds));                                            % log10(frequencies) and log10(ellip) used for KDE


[bandwidth,density_el,xmesh_el,cdf]=kde(log_ellip_dist,2^14);               % pdf on the log10(ellip)

[N,edges]=histcounts(log_ellip_dist,50,'Normalization','pdf');

[bandwidth_f,density_f,xmesh_f,cdf_f]=kde(log_f_dist,2^14);                 % pdf on the log10(frequency)

[N_f,edges_f]=histcounts(log_f_dist,50,'Normalization','pdf');


if strcmp(search,'O3')
    TT=readtable('./data/O3_GC_Upper_Limits_h095CL_LH.txt');

    fs_UL=TT.x_freq;
    h0s_UL=TT.h0_95;
elseif strcmp(search,'O3allsky')
    fid=fopen('./data/O3_FH_UL.txt');
    A = textscan(fid,'%s %f64');
    fs_UL=A{1};

    if iscell(fs_UL)
        fs_UL=str2double(fs_UL);                                                % just a check, to ensure we get an array of frequencies
    end

    h0s_UL=A{2};
    conserv_factor = 1.06;

    h0s_UL = h0s_UL * conserv_factor;                                       % really this is frequency and sky-dependent
end

fs_UL=fs_UL(fs_UL>=msp_min_fgw);                                            % selects only ULs above MSP min f GW
h0s_UL=h0s_UL(fs_UL>=msp_min_fgw);

ellip_UL=calc_ellipticity(h0s_UL,fs_UL,GC_dist,Izz)/UL_improvement_factor; % converts h0 UL to ellipticity ULs

ellip_kde=10.^xmesh_el(xmesh_f>=log10(msp_min_fgw));                        % just a check to ensure ellipticites taken at valid fs
ellip_pdf=density_el(xmesh_f>=log10(msp_min_fgw));

f_kde=10.^xmesh_f(xmesh_f>=log10(msp_min_fgw));                             % just a check to ensure frequencies are w/in MSP range
f_pdf=density_f(xmesh_f>=log10(msp_min_fgw));

Pgw=GeV_excess_calc_P_GW(fs_UL,ellip_UL,f_kde,f_pdf,ellip_kde,ellip_pdf);

end