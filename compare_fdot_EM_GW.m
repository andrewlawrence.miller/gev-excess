function [outputArg1,outputArg2] = compare_fdot_EM_GW(inputArg1,inputArg2)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
cc=constants;
% Msun=cc.Msun;
c=cc.c;
G=cc.G;
K=100; %% Bint=K Bext
R=10e3;
I=1e38;
msp_min_frot=60;                                                            % only consider MSPs as having rot freqs > this value

tbl=readtable('./data/pulsar_data_atnf_f0_fdots_dist_Bfield_type_age.csv',...    % read in f,fdot,Bext, age, distance, etc. from ATNF
    'NumHeaderLines',1);  
fs_dist=tbl.x_Hz_;
fdots_dist=abs(tbl.x_s__2_);

B_surface_fields=tbl.x_G_;                                        % external magnetic field of MSPs from ATNF, in Gauss
inds1=fs_dist>=msp_min_frot;

inds2=~isnan(B_surface_fields) & ~isnan(fdots_dist);

inds=inds1 & inds2;
B_surface_fields=B_surface_fields(inds);
fs_dist=fs_dist(inds);
fdots_dist=fdots_dist(inds);


epsilon=10^(-8)*(K*B_surface_fields./10^(12));
K_GW = 32*G / (5*c^5)* epsilon.^2 * I;
% 
fdot_GW=K_GW.*(2*fs_dist).^5 ./ (2*pi);

ratio=fdot_GW./abs(fdots_dist);

frac_energy=ratio.^2;
% 
% 
% ratio=omegadot_GW/omegadot_EM;

end